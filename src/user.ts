import { AppDataSource } from "./data-source";
import { Role } from "./entity/Role";
import { User } from "./entity/User";

AppDataSource.initialize()
  .then(async () => {
    const usersRepository = AppDataSource.getRepository(User);
    const roleReponsity = AppDataSource.getRepository(Role);
    const adminRole = await roleReponsity.findOneBy({ id: 1 });
    const userRole = await roleReponsity.findOneBy({ id: 2 });

    await usersRepository.clear();
    console.log("Inserting a new user into the Memory...");
    var user = new User();
    user.id = 1;
    user.email = "admin@email.com";
    user.password = "1234";
    user.gender = "male";
    user.roles = [];
    user.roles.push(adminRole);
    user.roles.push(userRole);
    console.log("Inserting a new user into the Database...");
    await usersRepository.save(user);

    console.log("Inserting a new user into the Memory...");
    var user = new User();
    user.id = 2;
    user.email = "user1@email.com";
    user.password = "1234";
    user.gender = "male";
    user.roles = [];
    user.roles.push(userRole);
    console.log("Inserting a new user into the Database...");
    await usersRepository.save(user);

    console.log("Inserting a new user into the Memory...");
    var user = new User();
    user.id = 3;
    user.email = "user2@email.com";
    user.password = "1234";
    user.gender = "female";
    user.roles = [];
    user.roles.push(userRole);
    console.log("Inserting a new user into the Database...");
    await usersRepository.save(user);

    // const user2 = await usersRepository.findOne({
    //     where: [{ id: 3 }, { gender: "female" }],
    //   });
    const users = await usersRepository.find({ relations: { roles: true } });
    console.log(JSON.stringify(users, null, 2));

    const rloes = await roleReponsity.find({ relations: { users: true } });
    console.log(JSON.stringify(rloes, null, 2));
  })
  .catch((error) => console.log(error));
